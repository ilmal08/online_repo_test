// language path
export 'l10n/l10n.dart';

// export path theme
export './src/theme/color_template.dart';
export './src/theme/size_screen_template.dart';

// export path common
export './src/common/common.dart';

// export path utility
export './src/utility/utility.dart';
