import 'package:flutter/material.dart';

class L10n {
  static final all = [
    const Locale('en'),
    const Locale('id'),
  ];

  static String getFlag(String code) {
    switch (code) {
      case 'id':
        return '🇦🇪';
      case 'en':
      default:
        return '🇺🇸';
    }
  }

  static String getNameLanguage(String code) {
    switch (code) {
      case 'id':
        return 'Indonesia';
      case 'en':
      default:
        return 'English';
    }
  }
}
