import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:online_repo_test/src/theme/color_template.dart';
import 'package:online_repo_test/src/theme/size_screen_template.dart';
import 'package:online_repo_test/src/utility/helper/language_helper_provider.dart';
import 'package:provider/provider.dart';

import '../../../l10n/l10n.dart';

class LanguagePickerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<LocaleProvider>(context);
    final locale = provider.locale;

    return DropdownButtonHideUnderline(
      child: Container(
        padding: EdgeInsets.only(left: 12, right: 12),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15.0),
          color: ThemeColors.background,
        ),
        child: DropdownButton(
          elevation: 0,
          value: locale,
          icon: Icon(
            Icons.arrow_drop_down,
            color: ThemeColors.secondary,
          ),
          items: L10n.all.map(
            (locale) {
              final flag = L10n.getNameLanguage(locale.languageCode);

              return DropdownMenuItem(
                child: Center(
                  child: Text(
                    flag,
                    style: TextStyle(
                        color: ThemeColors.secondary,
                        fontWeight: FontWeight.bold,
                        fontSize: ThemeSizes.fontLinkAction),
                  ),
                ),
                value: locale,
                onTap: () async {
                  final provider =
                      Provider.of<LocaleProvider>(context, listen: false);
                  provider.setLocale(locale);
                  print("cetak : " + locale.toString());
                },
              );
            },
          ).toList(),
          onChanged: (_) {},
        ),
      ),
    );
  }
}
