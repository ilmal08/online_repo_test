// components path
export './components/custom_switcher_template.dart';
export './components/listile_template.dart';
export './components/loader_template.dart';
export './components/template_dialog.dart';

// widgets path
export './widgets/language_picker.dart';
