// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors

import 'package:flutter/material.dart';

class ListileCustom extends StatelessWidget {
  final bool isSwitch;
  final String title;
  final IconData leadIcon;
  final IconData? trailingIcon;
  final VoidCallback? callback;
  final Widget? widgetCustom;
  const ListileCustom({
    required this.isSwitch,
    required this.title,
    required this.leadIcon,
    this.trailingIcon,
    this.callback,
    required this.widgetCustom,
  }) : super();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50,
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Icon(
              leadIcon,
              color: Colors.orange,
            ),
          ),
          Expanded(
            flex: 3,
            child: Text(
              title,
            ),
          ),
          isSwitch == false
              ? Expanded(
                  flex: 1,
                  child: Icon(
                    trailingIcon,
                    color: Colors.orange,
                  ),
                )
              : Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: widgetCustom,
                  ),
                ),
        ],
      ),
    );
  }
}
