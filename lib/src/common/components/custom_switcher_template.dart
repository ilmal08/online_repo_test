import 'package:flutter/material.dart';
import 'package:online_repo_test/online_repo_test.dart';

class CustomSwitch extends StatefulWidget {
  final bool value;
  final ValueChanged<bool> onChanged;
  final String? borderColorOn;
  final String? borderColorOff;
  final String? outlinedColorOn;
  final String? outlinedColorOff;
  final String? switchColorOn;
  final String? switchColorOff;

  CustomSwitch({
    required this.value,
    required this.onChanged,
    this.borderColorOn,
    this.borderColorOff,
    this.outlinedColorOn,
    this.outlinedColorOff,
    this.switchColorOn,
    this.switchColorOff,
  }) : super();

  @override
  _CustomSwitchState createState() => _CustomSwitchState();
}

class _CustomSwitchState extends State<CustomSwitch>
    with SingleTickerProviderStateMixin {
  late Animation _circleAnimation;
  late AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 60));
    _circleAnimation = AlignmentTween(
            begin: widget.value ? Alignment.centerRight : Alignment.centerLeft,
            end: widget.value ? Alignment.centerLeft : Alignment.centerRight)
        .animate(CurvedAnimation(
            parent: _animationController, curve: Curves.linear));
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) {
        return GestureDetector(
          onTap: () {
            if (_animationController.isCompleted) {
              _animationController.reverse();
            } else {
              _animationController.forward();
            }
            widget.value == false
                ? widget.onChanged(true)
                : widget.onChanged(false);
          },
          child: Container(
            width: 50.0,
            height: 20.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(24.0),
              color: widget.value
                  ? HexColor('${widget.borderColorOn}')
                  : HexColor('${widget.borderColorOff}'),
              border: Border.all(
                color: widget.value
                    ? HexColor('${widget.outlinedColorOn}')
                    : HexColor('${widget.outlinedColorOff}'),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 1, bottom: 1, right: 0.5, left: 0.5),
              child: Container(
                alignment:
                    widget.value ? Alignment.centerRight : Alignment.centerLeft,
                child: Container(
                  width: 20.0,
                  height: 20.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: widget.value
                        ? HexColor('${widget.switchColorOn}')
                        : HexColor('${widget.switchColorOff}'),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
