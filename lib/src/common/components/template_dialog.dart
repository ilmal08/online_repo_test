// ignore_for_file: prefer_const_constructors, unnecessary_string_interpolation
import 'package:flutter/material.dart';
import 'package:online_repo_test/online_repo_test.dart';

customDialogWithButton({
  required bool isTwoButton,
  required BuildContext context,
  required List<InlineSpan> text,
  required bool barrierDismissible,
  required String backgroundColor,
  required String barierColor,
  String? pathImage,
  String? button1Title,
  String? button1Color,
  Function? firstFunction,
  String? button2Title,
  String? button2Color,
  Function? secondFunction,
}) {
  WidgetsBinding.instance.addPostFrameCallback((_) async {
    showDialog(
      barrierDismissible: barrierDismissible,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: HexColor(backgroundColor),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          contentPadding: EdgeInsets.only(top: 10.0),
          content: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
              color: Colors.transparent,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                  child: Image.asset('$pathImage'),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 5, left: 15, right: 15, bottom: 10),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: RichText(
                          overflow: TextOverflow.visible,
                          textAlign: TextAlign.center,
                          text: TextSpan(
                              style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.black,
                                letterSpacing: 0.5,
                              ),
                              children: text),
                        ),
                      )
                    ],
                  ),
                ),
                isTwoButton == false
                    ? Padding(
                        padding: const EdgeInsets.all(10),
                        child: Row(
                          children: [
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  firstFunction!();
                                },
                                child: Container(
                                  padding:
                                      EdgeInsets.only(top: 10.0, bottom: 10.0),
                                  decoration: BoxDecoration(
                                    color: HexColor(button1Color ?? ''),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                  ),
                                  child: Text(
                                    button1Title ?? '',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 14,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    : Padding(
                        padding: const EdgeInsets.all(10),
                        child: Row(
                          children: [
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  firstFunction!();
                                },
                                child: Container(
                                  padding:
                                      EdgeInsets.only(top: 10.0, bottom: 10.0),
                                  decoration: BoxDecoration(
                                    color: HexColor(button1Color ?? ''),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10),
                                    ),
                                  ),
                                  child: Text(
                                    button1Title!,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 14,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(width: 10),
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  secondFunction!();
                                },
                                child: Container(
                                  padding:
                                      EdgeInsets.only(top: 10.0, bottom: 10.0),
                                  decoration: BoxDecoration(
                                    color: HexColor(button2Color ?? ''),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10),
                                    ),
                                  ),
                                  child: Text(
                                    button2Title ?? '',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 14,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
              ],
            ),
          ),
        );
      },
    );
  });
}
