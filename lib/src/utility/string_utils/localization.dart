import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class Localization {
  List<LocalizationsDelegate<dynamic>> localizationsDelegates =
      <LocalizationsDelegate<dynamic>>[
    AppLocalizations.delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ];

  String isRequired(BuildContext context) {
    return AppLocalizations.of(context)!.isRequaired;
  }

  String submit(BuildContext context) {
    return AppLocalizations.of(context)!.submit;
  }

  String existingCustomer(BuildContext context) {
    return AppLocalizations.of(context)!.existingCustomer;
  }

  String noRemitCardExisting(BuildContext context) {
    return AppLocalizations.of(context)!.noRemitCardExisting;
  }

  String numberIdCard(BuildContext context) {
    return AppLocalizations.of(context)!.numberIdCard;
  }

  String acceptTerms(BuildContext context) {
    return AppLocalizations.of(context)!.acceptTerms;
  }

  String checkBox1(BuildContext context) {
    return AppLocalizations.of(context)!.checkBox1;
  }

  String checkBox2(BuildContext context) {
    return AppLocalizations.of(context)!.checkBox2;
  }

  String backLable(BuildContext context) {
    return AppLocalizations.of(context)!.backLable;
  }

  String success(BuildContext context) {
    return AppLocalizations.of(context)!.succsess;
  }

  String messageExistingUser(BuildContext context) {
    return AppLocalizations.of(context)!.messageExistingUser;
  }

  String messageExistingUser2(BuildContext context) {
    return AppLocalizations.of(context)!.messageExistingUser2;
  }

  String notFullExistingOrdering(BuildContext context) {
    return AppLocalizations.of(context)!.notFullExistingOrdering;
  }

  String identityCard(BuildContext context) {
    return AppLocalizations.of(context)!.identityCard;
  }

  String typeOfId(BuildContext context) {
    return AppLocalizations.of(context)!.typeOfId;
  }

  String expiredIdCard(BuildContext context) {
    return AppLocalizations.of(context)!.expiredIdCard;
  }

  String personalIdentity(BuildContext context) {
    return AppLocalizations.of(context)!.personalIdentity;
  }

  String customerType(BuildContext context) {
    return AppLocalizations.of(context)!.customerType;
  }

  String nickname(BuildContext context) {
    return AppLocalizations.of(context)!.nickname;
  }

  String fullName(BuildContext context) {
    return AppLocalizations.of(context)!.fullName;
  }

  String emailLabel(BuildContext context) {
    return AppLocalizations.of(context)!.emailLabel;
  }

  String dateOfBirth(BuildContext context) {
    return AppLocalizations.of(context)!.dateOfBirth;
  }

  String genderLabel(BuildContext context) {
    return AppLocalizations.of(context)!.genderLabel;
  }

  String nationality(BuildContext context) {
    return AppLocalizations.of(context)!.nationality;
  }

  String address1(BuildContext context) {
    return AppLocalizations.of(context)!.address1;
  }

  String postalCode(BuildContext context) {
    return AppLocalizations.of(context)!.postalCode;
  }

  String mobilePhoneNumber(BuildContext context) {
    return AppLocalizations.of(context)!.mobilePhoneNumber;
  }

  String faxNumber(BuildContext context) {
    return AppLocalizations.of(context)!.faxNumber;
  }

  String visaLabel(BuildContext context) {
    return AppLocalizations.of(context)!.visaLabel;
  }

  String occupationLabel(BuildContext context) {
    return AppLocalizations.of(context)!.occupationLabel;
  }

  String permissiontoLand(BuildContext context) {
    return AppLocalizations.of(context)!.permissiontoLand;
  }

  String periodetoStay(BuildContext context) {
    return AppLocalizations.of(context)!.periodetoStay;
  }

  String companyLabel(BuildContext context) {
    return AppLocalizations.of(context)!.companyLabel;
  }

  String companyName(BuildContext context) {
    return AppLocalizations.of(context)!.companyName;
  }

  String companyAddress1(BuildContext context) {
    return AppLocalizations.of(context)!.companyAddress1;
  }

  String companyAddress2(BuildContext context) {
    return AppLocalizations.of(context)!.companyAddress2;
  }

  String companyAddress3(BuildContext context) {
    return AppLocalizations.of(context)!.companyAddress3;
  }

  String companyPhoneNumber(BuildContext context) {
    return AppLocalizations.of(context)!.companyPhoneNumber;
  }

  String companyFaxNumber(BuildContext context) {
    return AppLocalizations.of(context)!.companyFaxNumber;
  }

  String accountNumber(BuildContext context) {
    return AppLocalizations.of(context)!.accountNumber;
  }

  String bankNameCliring(BuildContext context) {
    return AppLocalizations.of(context)!.bankNameCliring;
  }

  String bankNameRTGS(BuildContext context) {
    return AppLocalizations.of(context)!.bankNameRTGS;
  }

  String bankNameInterbank(BuildContext context) {
    return AppLocalizations.of(context)!.bankNameInterbank;
  }

  String bankAddressLabel(BuildContext context) {
    return AppLocalizations.of(context)!.bankAddressLabel;
  }

  String codeLabel(BuildContext context) {
    return AppLocalizations.of(context)!.codeLabel;
  }

  String registerBeneficiarySuccsess(BuildContext context) {
    return AppLocalizations.of(context)!.registerBeneficiarySuccsess;
  }

  String registerBeneficiarySuccsess2(BuildContext context) {
    return AppLocalizations.of(context)!.registerBeneficiarySuccsess2;
  }

  String addNewBenef(BuildContext context) {
    return AppLocalizations.of(context)!.addNewBenef;
  }

  String benefInformation(BuildContext context) {
    return AppLocalizations.of(context)!.benefInformation;
  }

  String recipientName(BuildContext context) {
    return AppLocalizations.of(context)!.recipientName;
  }

  String recipientFullName(BuildContext context) {
    return AppLocalizations.of(context)!.recipientFullName;
  }

  String recipientAddress(BuildContext context) {
    return AppLocalizations.of(context)!.recipientAddress;
  }

  String benefPhoneNumber(BuildContext context) {
    return AppLocalizations.of(context)!.benefPhoneNumber;
  }

  String recepientEmail(BuildContext context) {
    return AppLocalizations.of(context)!.recepientEmail;
  }

  String recepientRelation(BuildContext context) {
    return AppLocalizations.of(context)!.recepientRelation;
  }

  String sourceofFunds(BuildContext context) {
    return AppLocalizations.of(context)!.sourceofFunds;
  }

  String purposeofRemit(BuildContext context) {
    return AppLocalizations.of(context)!.purposeofRemit;
  }

  String recipientResident(BuildContext context) {
    return AppLocalizations.of(context)!.recipientResident;
  }

  String recipientNote(BuildContext context) {
    return AppLocalizations.of(context)!.recipientNote;
  }

  String currency(BuildContext context) {
    return AppLocalizations.of(context)!.currency;
  }

  String serviceType(BuildContext context) {
    return AppLocalizations.of(context)!.serviceType;
  }

  String recipientIdType(BuildContext context) {
    return AppLocalizations.of(context)!.recipientIdType;
  }

  String maleLabel(BuildContext context) {
    return AppLocalizations.of(context)!.maleLabel;
  }

  String femaleLabel(BuildContext context) {
    return AppLocalizations.of(context)!.femaleLabel;
  }

  String orderingCustomerTitle(BuildContext context) {
    return AppLocalizations.of(context)!.orderingCustomerTitle;
  }

  String pictureOfIdCard(BuildContext context) {
    return AppLocalizations.of(context)!.pictureOfIdCard;
  }

  String address2(BuildContext context) {
    return AppLocalizations.of(context)!.address2;
  }

  String address3(BuildContext context) {
    return AppLocalizations.of(context)!.address3;
  }

  String phoneNumber(BuildContext context) {
    return AppLocalizations.of(context)!.phoneNumber;
  }

  String signature(BuildContext context) {
    return AppLocalizations.of(context)!.signature;
  }

  String activationCode(BuildContext context) {
    return AppLocalizations.of(context)!.activationCode;
  }

  String rememberPassword(BuildContext context) {
    return AppLocalizations.of(context)!.rememberPassword;
  }

  String login(BuildContext context) {
    return AppLocalizations.of(context)!.login;
  }

  String forgorPasswordLabel(BuildContext context) {
    return AppLocalizations.of(context)!.forgorPasswordLabel;
  }

  String forgotPasswordLabelDetail(BuildContext context) {
    return AppLocalizations.of(context)!.forgotPasswordLabelDetail;
  }

  String receivedAsk(BuildContext context) {
    return AppLocalizations.of(context)!.receivedAsk;
  }

  String activateNow(BuildContext context) {
    return AppLocalizations.of(context)!.activateNow;
  }

  String newCustomer(BuildContext context) {
    return AppLocalizations.of(context)!.newCustomer;
  }

  String register(BuildContext context) {
    return AppLocalizations.of(context)!.register;
  }

  String closeApp(BuildContext context) {
    return AppLocalizations.of(context)!.closeApp;
  }

  String closeAppDetail(BuildContext context) {
    return AppLocalizations.of(context)!.closeAppDetail;
  }

  String yes(BuildContext context) {
    return AppLocalizations.of(context)!.yes;
  }

  String cancel(BuildContext context) {
    return AppLocalizations.of(context)!.cancel;
  }

  String watermarkBNI(BuildContext context) {
    return AppLocalizations.of(context)!.watermarkBNI;
  }

  String username(BuildContext context) {
    return AppLocalizations.of(context)!.username;
  }

  String password(BuildContext context) {
    return AppLocalizations.of(context)!.password;
  }

  String forgotPassword(BuildContext context) {
    return AppLocalizations.of(context)!.forgotPassword;
  }

  String didntReceiveLink(BuildContext context) {
    return AppLocalizations.of(context)!.didntReceiveLink;
  }

  String resend(BuildContext context) {
    return AppLocalizations.of(context)!.resend;
  }

  String forgotPasswordSuccessDetail(BuildContext context) {
    return AppLocalizations.of(context)!.forgotPasswordSuccessDetail;
  }

  String forgotSuccess(BuildContext context) {
    return AppLocalizations.of(context)!.forgotSuccess;
  }

  String passwordNotMatch(BuildContext context) {
    return AppLocalizations.of(context)!.passwordNotMatch;
  }

  String confirmPassword(BuildContext context) {
    return AppLocalizations.of(context)!.confirmPassword;
  }

  String retypePassword(BuildContext context) {
    return AppLocalizations.of(context)!.retypePassword;
  }

  String resetPassword(BuildContext context) {
    return AppLocalizations.of(context)!.resetPassword;
  }

  String greetingLabel(BuildContext context) {
    return AppLocalizations.of(context)!.greetingLabel;
  }

  String logoutLabel(BuildContext context) {
    return AppLocalizations.of(context)!.logoutLabel;
  }

  String profilLabel(BuildContext context) {
    return AppLocalizations.of(context)!.profilLabel;
  }

  String accountProfil(BuildContext context) {
    return AppLocalizations.of(context)!.accountProfil;
  }

  String beneficiaryManagement(BuildContext context) {
    return AppLocalizations.of(context)!.beneficiaryManagement;
  }

  String beneficiaryManagementDetail(BuildContext context) {
    return AppLocalizations.of(context)!.beneficiaryManagementDetail;
  }

  String nameLabel(BuildContext context) {
    return AppLocalizations.of(context)!.nameLabel;
  }

  String beneficiaryIdLabel(BuildContext context) {
    return AppLocalizations.of(context)!.beneficiaryIdLabel;
  }

  String manageBeneficiariesLabel(BuildContext context) {
    return AppLocalizations.of(context)!.manageBeneficiariesLabel;
  }

  String pageNotFound(BuildContext context) {
    return AppLocalizations.of(context)!.pageNotFound;
  }
}
