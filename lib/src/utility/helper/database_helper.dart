import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class DatabaseHelper {
  final FlutterSecureStorage storage = const FlutterSecureStorage();

  Future writeSecureData(String key, String value) async {
    var writeData = await storage.write(key: key, value: value);
    return writeData;
  }

  Future readSecureData(String key) async {
    var readData = await storage.read(key: key);
    return readData;
  }

  Future deleteAllSecureData() async {
    var deleteData = await storage.deleteAll();
    return deleteData;
  }
}
