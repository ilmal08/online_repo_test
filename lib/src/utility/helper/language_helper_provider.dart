import 'package:flutter/material.dart';
import 'package:online_repo_test/src/utility/helper/database_helper.dart';

import '../../../l10n/l10n.dart';

class LocaleProvider extends ChangeNotifier {
  late Locale _locale = Locale('en');
  late String createToken = '';
  late String activationCode = '';

  Locale get locale => _locale;

  void setLocale(Locale locale) async {
    if (!L10n.all.contains(locale)) return;
    _locale = locale;

    await DatabaseHelper().writeSecureData('langLocale', locale.toString());
    notifyListeners();
  }

  void setCreateToken(String token) async {
    createToken = token;
    await DatabaseHelper().writeSecureData('token', token);
    notifyListeners();
  }

  void clearLocale() {
    notifyListeners();
  }
}
