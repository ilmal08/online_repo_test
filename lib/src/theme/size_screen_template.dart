import 'package:flutter_screenutil/flutter_screenutil.dart';

class ThemeSizes {
  //margin
  static double baseLargeMargin = 32;
  static double baseMediumMargin = 24;
  static double baseHalfMediumMargin = 12;
  static double baseMargin = 16;
  static double baseSmallMargin = 8;

  //sizedBox
  static double boxMedium = 24.h;
  static double boxStandart = 16.h;
  static double boxSmall = 4.h;

  //button
  static double buttonActionIntro = 48.h;
  static double buttonAction = 40.h;

  //image or background
  static double imageBackgroundIntro = 190.h;
  static double imageWaterMarkBNI = 36.h;

  //radius border
  static double radiusBackgroundIntro = 32.h;
  static double radiusButton = 16.h;
  static double radiusCard = 12;

  //icon
  static double iconBase = 16;
  static double iconInAppBar = 26;
  static double iconCard = 36;

  //font
  static double fontHeadlineIntro = 40.sp;
  static double fontHeadline = 26.sp;
  static double fontSmallHeadline = 20.sp;
  static double fontTitle = 16.sp;
  static double fontFieldText = 14.sp;
  static double fontSubheader = 12.sp; //bold
  static double fontBodyOrMenu = 12.sp;
  static double fontCaption = 10.sp;

  static double fontButtonAction = 14.sp;
  static double fontLinkAction = 12.sp;
  static double fontLinkActionMini = 10.sp;

  //other
  static double heightInputField = 48;
  static double lottiLeft = 70.w;
  static double lottiTop = 150.h;
}
