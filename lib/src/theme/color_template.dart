import 'package:flutter/material.dart';

class ThemeColors {
  static const Color statusSuccess = Color(0xff8ac248); //hijau
  static const Color statusProsessing = Color(0xffface7f); //kuning
  static const Color statusPending = Color(0xfffbb54e); //kuning
  static const Color statusRejected = Color(0xffde4436); //merah

  static const Color background = Color(0xFFF5F5F5);
  static const Color onBackground = Color(0xFF000000);
  static const Color backgroundLight = Color(0xffffffff);

  static const Color primary = Color(0xFF17B3BF);
  static const Color primaryVariant = Color(0xFF128891);
  static const Color onPrimary = Color(0xffffffff);

  static const Color secondary = Color(0xFFF15A23);
  static const Color secondaryVariant = Color(0xFFD4420D);
  static const Color onSecondaryVariant = Color(0xffffffff);

  static const Color accentGray = Color(0XFF888888);
  static const Color accentGrayLight = Color(0xffe2e2e2);

  static const Color error = Color(0xFFB00020);

  static const MaterialColor buttonTextColor =
      MaterialColor(0xFF17B3BF, <int, Color>{
    50: Color(0xFFD1F7FA),
    100: Color(0xFFD1F7FA),
    200: Color(0xFFA4EEF4),
    300: Color(0xFF76E6EF),
    400: Color(0xFF49DEE9),
    500: Color(0xFF1BD5E4),
    600: Color(0xFF17B3BF),
    700: Color(0xFF197880),
    800: Color(0xFF115055),
    900: Color(0xFF032D30),
  });
}
